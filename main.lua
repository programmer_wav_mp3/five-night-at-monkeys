
require("menu")
require("game")

function love.load()
    gameState = 1

    MenuLoad()
    GameLoad()
    endGameLoad()
end

function love.update(dt)
    if gameState == 1 then MenuUpdate(dt) end
    if gameState == 2 then GameUpdate(dt) end
    if gameState == 3 then EndUpdate(dt) end
end

function love.draw()
    if gameState == 1 then MenuDraw() end
    if gameState == 2 then GameDraw() end
    if gameState == 3 then EndDraw() end
end
