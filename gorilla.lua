function LoadGorilla()
    Gorilla = love.graphics.newImage("assets/Gorilla.png")
    Gorilla2 = love.graphics.newImage("assets/Gorilla2.png")
    Gorilla3 = love.graphics.newImage("assets/Gorilla3.png")
    Gorilla4 = love.graphics.newImage("assets/Gorilla4.png")
    Gorilla5 = love.graphics.newImage("assets/Gorilla5.png")
    
    AI4 = {}
    AI4.Difficulty = 0
    AI4.CurrentRoom = 12
    AI4.TargetRoom = 7
    AI4.DefaultTimer = 7
    AI4.OpportunityTimer = AI4.DefaultTimer
    AI4.MovementTimerDefault = 5
    AI4.MovementTimer = 5
    AI4.SucceededMovement = false
    AI4.GorillaState = 1
    AI4.RanInHallway = false
    AI4.GorillaSpeed = 800
    AI4.GorillaDistanceTravelled = 0
end

function UpdateGorilla(dt)
    if Power > 0 then
        MoveOpportunity4(dt)
        AI4.GorillaSpeed = 900 * dt
    else
        GorillaState = 0
    end
end

function MoveOpportunity4(dt)

    if AI4.OpportunityTimer <= 0 then
        if AI4.Difficulty >= love.math.random(1, 20) then
            AI4.SucceededMovement = true
        else
            AI4.OpportunityTimer = AI4.DefaultTimer
        end
    end

    if AI4.SucceededMovement == true then
        
        if AI4.GorillaState < 6 then 
            AI4.GorillaState = AI4.GorillaState + 1

            if AI4.GorillaState < 6 then
                ResetMovementOpportunity4()
            end
        end

        if AI4.GorillaState > 5 then
            AI4.MovementTimer = AI4.MovementTimer - dt
            ChangeLocation4(AI4.CurrentRoom, AI4.TargetRoom)
        end

    else
        if CurrentCam == AI4.CurrentRoom and CamState == true then
            AI4.OpportunityTimer = AI4.OpportunityTimer - dt / 4
        else
            AI4.OpportunityTimer = AI4.OpportunityTimer - dt
        end
    end

    if AI4.MovementTimer <= 0 and LeftDoorClosed == false then
        KillPlayer("Gorilla")
    elseif AI4.MovementTimer <= 0 then
        AI4.GorillaState = 1
        ChangeLocation4(AI4.CurrentRoom, 12)
        ResetMovementOpportunity4()
    end

    function ResetMovementOpportunity4()
        AI4.DefaultTimer = (love.math.random(36, 66) / 6) -- 6 to 11 seconds
        AI4.OpportunityTimer = AI4.DefaultTimer
        AI4.MovementTimer = AI4.MovementTimerDefault
        AI4.SucceededMovement = false
        AI4.RanInHallway = false
        AI4.GorillaDistanceTravelled = 0
    end
    
    function ChangeLocation4(StartingLocation4, TargetLocation4)
            AI4.CurrentRoom = TargetLocation4
    end

    function DrawDebug4()
        love.graphics.setColor(1, 0, 0)
        love.graphics.print(AI4.OpportunityTimer, 900, 400)
        love.graphics.print(AI4.MovementTimer, 900, 410)
        love.graphics.print(AI4.CurrentRoom, 900, 600, 0, 2, 2)
        love.graphics.print(AI4.TargetRoom, 950, 600, 0, 2, 2)
        love.graphics.print(AI4.GorillaState, 1000, 600, 0, 2, 2)
        love.graphics.setColor(1, 1, 1)
    end
end