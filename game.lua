require("endGame")
require("baboon1")
require("baboon2")
require("capuchin")
require("gorilla")

function GameLoad()
    Screen_Size = love.window.setMode(1280, 720)
    CamLoad()
    OfficeLoad()
    LoadBaboon()
    LoadBaboon2()
    LoadCapuchin()
    LoadGorilla()
    CamMonkeyLoad()
    Dead = false
    Win = false
    LeftDoorClosed = false
    LeftDoorElectrified = false
    RightDoorClosed = false
    RightDoorElectrified = false
    Hour = 0 -- Time progresses until 6 am (60 units of time per hour)
    Power = 400
    PowerConsumptionLevel = 0.25
    love.audio.setVolume(volumeValue)
    mouse_down = false
    CalendarEasterEgg = 0 
    PowerOutagePhase1Timer = 20
    PowerOutagePhase2Timer = 20
    PowerOutagePhase3Timer = 10
end

function GameUpdate(dt)
    
    if Dead == true or Win == true then 
        gameState = 3 
    end

    if Dead == false then
        UpdateBaboon(dt)
        UpdateBaboon2(dt)
        UpdateCapuchin(dt)
        UpdateGorilla(dt)

        if Power > 0 then
            Power = Power - (PowerConsumptionLevel * dt)
            UpdatePowerConsumptionLevel()
        else
            
            if PowerOutagePhase1Timer > 0 then
                PowerOutagePhase1Timer = PowerOutagePhase1Timer - dt
            elseif PowerOutagePhase1Timer <= 0 and PowerOutagePhase2Timer > 0 then
                PowerOutagePhase2Timer = PowerOutagePhase2Timer - dt

                if CapuchinEyes:isPlaying() == false then
                    CapuchinEyes:rewind()
                    CapuchinEyes:play()
                end
        
            elseif PowerOutagePhase2Timer <= 0 and PowerOutagePhase3Timer > 0 then
                PowerOutagePhase3Timer = PowerOutagePhase3Timer - dt
            elseif PowerOutagePhase3Timer <= 0 then
                KillPlayer("Capuchin")
                return
            end
            
            if PowerConsumptionLevel > 0 then
                PowerOutagePhase1Timer = math.random(10, 25)
                PowerOutagePhase2Timer = math.random(15, 25)
                PowerOutagePhase3Timer = math.random(5, 15)
                PowerConsumptionLevel = 0
            end
            
            if CamState == true then
                CamState = false
            end
        end

        Hour = Hour + dt
       
        mouse_x = love.mouse.getX()
        mouse_y = love.mouse.getY()
        mouse_width = 1
        mouse_height = 1

        if CamState == false  then
            if love.keyboard.isDown("f") then
                if Power > 0 then
                    LightSwitch = true
                else
                    -- play distressing sound effect but do nothing
                end
            else
                LightSwitch = false
                if love.mouse.isDown(1) then
                    if mouse_down == not mouse_down then
                        detectCollision()
                        mouse_down = true
                    end
                else
                    mouse_down = false
                end
            end
        end

        if ScreenStatic:isPlaying() == false then
            ScreenStatic:rewind()
            ScreenStatic:play()
        end 
    end
end

function GameDraw()
    if Dead == false and Win == false then
        DrawBaboon()
        DrawBaboon2()
        OfficePreDraw()
        DrawDebug() 
        DrawDebug2()
        DrawDebug3()
        DrawDebug4()
        CamDraw()
        OfficePostDraw()
    end
end

-- Load Functions
function OfficeLoad()
    Office = love.graphics.newImage("assets/office.png")
    OfficeDarkness = love.graphics.newImage("assets/Darkness.png")
    OfficeDarkness2 = love.graphics.newImage("assets/Darkness2.png")
    LeftSwitchUp = love.graphics.newImage("assets/LeftSwitchUp.png")
    LeftSwitchDown = love.graphics.newImage("assets/LeftSwitchDown.png")
    RightSwitchUp = love.graphics.newImage("assets/RightSwitchUp.png")
    RightSwitchDown = love.graphics.newImage("assets/RightSwitchDown.png")
    LeftDoor = love.graphics.newImage("assets/LeftDoorClosed.png")
    RightDoor = love.graphics.newImage("assets/RightDoorClosed.png")
    Calendar1 = love.graphics.newImage("assets/Calendar_1.png") -- easter egg that appears randomly
    Calendar2 = love.graphics.newImage("assets/Calendar_2.png") -- easter egg that appears randomly
    CapuchinEyes = love.graphics.newVideo("assets/CapuchinEyes.ogv")
end

function CamLoad()
    CurrentCam = 1
    CamState = false

    Cam1 = love.graphics.newImage("assets/Cams/Cam1/Hallway_1.png")

    Cam2 = love.graphics.newImage("assets/Cams/Cam2/Hallway_2.png")

    Cam3 = love.graphics.newImage("assets/Cams/Cam3/Test_Cam3.png")

    Cam4Back = love.graphics.newImage("assets/Cams/Cam4/Fountain_Back_1.png")
    Cam4Back2 = love.graphics.newImage("assets/Cams/Cam4/Fountain_Back_2.png")

    -- Cam 5 is static and audio only

    Cam6 = love.graphics.newImage("assets/Cams/Cam6/Food_Court_Back.png")

    Cam7Back = love.graphics.newImage("assets/Cams/Cam7/Power_Room_Back.png")
    Cam7Mid = love.graphics.newImage("assets/Cams/Cam7/Power_Room_Mid.png")
    Cam7Front = love.graphics.newImage("assets/Cams/Cam7/Power_Room_Front.png")

    Cam8 = love.graphics.newImage("assets/Cams/Cam8/Lost_and_Found.png")

    Cam9Back = love.graphics.newImage("assets/Cams/Cam9/Tropical_Enclosure_Back.png")
    Cam9Mid = love.graphics.newImage("assets/Cams/Cam9/Tropical_Enclosure_Mid_1.png")
    Cam9Mid2 = love.graphics.newImage("assets/Cams/Cam9/Tropical_Enclosure_Mid_2.png")
    Cam9Front = love.graphics.newImage("assets/Cams/Cam9/Tropical_Enclosure_Front_1.png")
    Cam9Front2 = love.graphics.newImage("assets/Cams/Cam9/Tropical_Enclosure_Front_2.png")

    Cam10Back = love.graphics.newImage("assets/Cams/Cam10/Savannah_Enclosure_Back.png")
    Cam10Mid = love.graphics.newImage("assets/Cams/Cam10/Savannah_Enclosure_Mid.png")
    Cam10Front = love.graphics.newImage("assets/Cams/Cam10/Savannah_Enclosure_Front_1.png")
    Cam10Front2 = love.graphics.newImage("assets/Cams/Cam10/Savannah_Enclosure_Front_2.png")

    CamBorder = love.graphics.newImage("assets/Cams/CamBorder.png")
    ScreenStatic = love.graphics.newVideo("assets/Cams/Static.ogv")
    CamHUD = love.graphics.newImage("assets/Cams/Cam_HUD.png")

    CamButtonList = {}
    --Caméra 1, Hallway #1--
    CamButtonList.Cam1 = {}
    CamButtonList.Cam1.x = 920
    CamButtonList.Cam1.y = 590
    --Caméra 2, Hallway #2--
    CamButtonList.Cam2 = {}
    CamButtonList.Cam2.x = 1035
    CamButtonList.Cam2.y = 580
    --Caméra 3, Souvenir Shop--
    CamButtonList.Cam3 = {}
    CamButtonList.Cam3.x = 823
    CamButtonList.Cam3.y = 505
    --Caméra 4, Fountain--
    CamButtonList.Cam4 = {}
    CamButtonList.Cam4.x = 930
    CamButtonList.Cam4.y = 435
    --Caméra 5, Observatory--
    CamButtonList.Cam5 = {}
    CamButtonList.Cam5.x = 1035
    CamButtonList.Cam5.y = 445
    --Caméra 6, Food Court--
    CamButtonList.Cam6 = {}
    CamButtonList.Cam6.x = 823
    CamButtonList.Cam6.y = 360
    --Caméra 7, Power Room--
    CamButtonList.Cam7 = {}
    CamButtonList.Cam7.x = 930
    CamButtonList.Cam7.y = 300
    --Caméra 8, Lost and Found--
    CamButtonList.Cam8 = {}
    CamButtonList.Cam8.x = 770
    CamButtonList.Cam8.y = 577
    --Caméra 9, Baboon Enclosure #1--
    CamButtonList.Cam9 = {}
    CamButtonList.Cam9.x = 785
    CamButtonList.Cam9.y = 300
    --Caméra 10, Baboon Enclosure #2--
    CamButtonList.Cam10 = {}
    CamButtonList.Cam10.x = 1100
    CamButtonList.Cam10.y = 350

    CamButtonList.width = 65
    CamButtonList.height = 30
    CamButtonList.halfwidth = CamButtonList.width / 2
    CamButtonList.halfheight = CamButtonList.height / 2
end

-- Draw Functions
function OfficePreDraw()

    if Power > 0 then
        if LeftDoorClosed == true then love.graphics.draw(LeftDoor) end
        if RightDoorClosed == true then love.graphics.draw(RightDoor) end
    end

     if Power <= 0 and PowerOutagePhase1Timer <= 0 and PowerOutagePhase2Timer > 0 then
         love.graphics.draw(CapuchinEyes)
    end
   
    love.graphics.draw(Office, 0, 0, 0)

    love.graphics.print(PowerOutagePhase1Timer, 333, 300, 0, 2, 2)
    love.graphics.print(PowerOutagePhase2Timer, 444, 400, 0, 2, 2)
    love.graphics.print(PowerOutagePhase3Timer, 555, 500, 0, 2, 2)

    if LeftDoorElectrified == true then love.graphics.draw(LeftSwitchDown) else love.graphics.draw(LeftSwitchUp) end
    if RightDoorElectrified == true then love.graphics.draw(RightSwitchDown) else love.graphics.draw(RightSwitchUp) end

    if CalendarEasterEgg > -1 and CalendarEasterEgg < 5 then
        love.graphics.draw(Calendar1)
    elseif CalendarEasterEgg > 4 and CalendarEasterEgg < 9 then
        love.graphics.draw(Calendar2)
    end
   
    if love.keyboard.isDown("h") then
        Power = 0
    end
        
    if Power <= 0 then
        love.graphics.setColor(0, 0, 0.3, 0.5)
        love.graphics.rectangle("fill", 0, 0, 1280, 720)
        love.graphics.draw(OfficeDarkness, 0, 0, 0)
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.draw(OfficeDarkness2, 0, -200, 0)
    else
        love.graphics.setColor(1, 1, 1, 0.75)
        love.graphics.draw(OfficeDarkness, 0, 0, 0)
        love.graphics.setColor(1, 1, 1, 1)
    end

    if LightSwitch == true then
        love.graphics.setColor(1, 1, 1, 0.5)
        love.graphics.circle("fill", 80, 400, 80, 100)
        love.graphics.circle("fill", 1200, 400, 80, 100)
        love.graphics.setColor(1, 1, 1, 1)
    end

   
end

function OfficePostDraw()

    love.graphics.setColor(0, 0, 1)
    if Hour < 60 and Hour > 0 then love.graphics.print("12:00 AM", 1000, 0, 0, 4, 4)  
        elseif Hour < 120 and Hour > 60 then love.graphics.print("1:00 AM", 1000, 0, 0, 4, 4)  
        elseif Hour < 180 and Hour > 120 then love.graphics.print("2:00 AM", 1000, 0, 0, 4, 4)  
        elseif Hour < 240 and Hour > 180 then love.graphics.print("3:00 AM", 1000, 0, 0, 4, 4)  
        elseif Hour < 300 and Hour > 240 then love.graphics.print("4:00 AM", 1000, 0, 0, 4, 4)  
        elseif Hour < 360 and Hour > 300 then love.graphics.print("5:00 AM", 1000, 0, 0, 4, 4)  
        elseif Hour > 360 then Win = true
    end
     love.graphics.setColor(1, 1, 0)
     love.graphics.print(math.ceil(Power * 100 / 400), 0, 600, 0, 4, 4)
     love.graphics.print(PowerConsumptionLevel, 0, 650, 0, 4, 4)
     love.graphics.setColor(1, 1, 1)
end

function CamDraw()
    if CamState == true then

        DrawCamButtons(CamButtonList.Cam1.x, CamButtonList.Cam1.y, 6) -- Hallway 1
        DrawCamButtons(CamButtonList.Cam2.x, CamButtonList.Cam2.y, 7) -- Hallway 2
        DrawCamButtons(CamButtonList.Cam3.x, CamButtonList.Cam3.y, 5) -- Souvenir Shop
        DrawCamButtons(CamButtonList.Cam4.x, CamButtonList.Cam4.y, 3) -- Fountain
        DrawCamButtons(CamButtonList.Cam5.x, CamButtonList.Cam5.y, 4) -- Observatory
        DrawCamButtons(CamButtonList.Cam6.x, CamButtonList.Cam6.y, 2) -- Food Court
        DrawCamButtons(CamButtonList.Cam7.x, CamButtonList.Cam7.y, 1) -- Power
        DrawCamButtons(CamButtonList.Cam8.x, CamButtonList.Cam8.y, 12) -- Lost and Found
        DrawCamButtons(CamButtonList.Cam9.x, CamButtonList.Cam9.y, 10) -- Tropical
        DrawCamButtons(CamButtonList.Cam10.x, CamButtonList.Cam10.y, 11) -- Savannah 

        love.graphics.draw(ScreenStatic, 0, 0, 0)

        CamPreDraw()

        CamPostDraw()

        -- Cam names HUD
        if CurrentCam == 1 then
            love.graphics.print("Power Room", 900, 600, 0, 4, 4)
        end

        if CurrentCam == 2 then
            love.graphics.print("Food Court", 900, 600, 0, 4, 4)
        end

        if CurrentCam == 3 then
            love.graphics.print("Fountain", 900, 600, 0, 4, 4)
        end

        if CurrentCam == 4 then
            love.graphics.print("Observatory", 900, 600, 0, 4, 4)
        end

        if CurrentCam == 5 then
            love.graphics.print("Souvenir Shop", 890, 600, 0, 4, 4)
        end

        if CurrentCam == 6 then
            love.graphics.print("Left Hallway", 900, 600, 0, 4, 4)
        end

        if CurrentCam == 7 then
            love.graphics.print("Right Hallway", 900, 600, 0, 4, 4)
        end

        if CurrentCam == 10 then
            love.graphics.print("Tropical Enclosure", 850, 600, 0, 4, 4)
        end

        if CurrentCam == 11 then
            love.graphics.print("Savannah Enclosure ", 850, 600, 0, 4, 4)
        end
        
        if CurrentCam == 12 then
            love.graphics.print("Lost and Found", 850, 600, 0, 4, 4)
        end 
    end
end

function CamPreDraw()  -- draws stuff on the backmost layer
    if CurrentCam == 6 then
        love.graphics.draw(Cam1, 25, 25, 0, 0.96, 0.88)
    elseif CurrentCam == 7 then
        love.graphics.draw(Cam2, 25, 25, 0, 0.96, 0.88)
    elseif CurrentCam == 5 then
        love.graphics.draw(Cam3, 25, 25, 0, 0.96, 0.88)
    elseif CurrentCam == 3 then
        if AI3.CurrentRoom == 3 then
            love.graphics.draw(Cam4Back2, 25, 25, 0, 0.96, 0.88)
        else
            love.graphics.draw(Cam4Back, 25, 25, 0, 0.96, 0.88)
        end
    elseif CurrentCam == 2 then -- cam 5 is static and audio only
        love.graphics.draw(Cam6, 25, 25, 0, 0.68, 0.6)
    elseif CurrentCam == 1 then
        love.graphics.draw(Cam7Back, 25, 25, 0, 0.96, 0.88)
        love.graphics.draw(Cam7Mid, 25, 25, 0, 0.96, 0.88)
    elseif CurrentCam == 12 then
        love.graphics.draw(Cam8, 25, 25, 0, 0.96, 0.88)
    elseif CurrentCam == 11 then
        love.graphics.draw(Cam9Back, 25, 25, 0, 0.96, 0.88)
    elseif CurrentCam == 10 then
        love.graphics.draw(Cam10Back, 25, 25, 0, 0.96, 0.88)
    end
end

function CamPostDraw() -- draws stuff above the backmost layer
    CamMonkeyDraw()

    if CurrentCam == 6 then

    elseif CurrentCam == 7 then

    elseif CurrentCam == 5 then

    elseif CurrentCam == 3 then

    elseif CurrentCam == 4 then

    elseif CurrentCam == 2 then

    elseif CurrentCam == 1 then
        love.graphics.draw(Cam7Front, 25, 25, 0, 0.96, 0.88)
    elseif CurrentCam == 12 then
 
    elseif CurrentCam == 11 then
        love.graphics.draw(Cam9Mid, 25, 25, 0, 0.96, 0.88) 
        love.graphics.draw(Cam9Mid2, 25, 25, 0, 0.96, 0.88)
        if AI2.MovedOnce == false then
            love.graphics.draw(Cam9Front, 25, 25, 0, 0.96, 0.88)
        else
            love.graphics.draw(Cam9Front2, 25, 25, 0, 0.96, 0.88)
        end
    elseif CurrentCam == 10 then
        love.graphics.draw(Cam10Mid, 25, 25, 0, 0.96, 0.88)
        if AI.MovedOnce == false then
            love.graphics.draw(Cam10Front, 25, 25, 0, 0.96, 0.88)
        else
            love.graphics.draw(Cam10Front2, 25, 25, 0, 0.96, 0.88)
        end
    end

    -- VFX & Aesthetics
    love.graphics.draw(OfficeDarkness, 0, 0, 0)
    love.graphics.setColor(1, 1, 1, 0.25)
    love.graphics.draw(ScreenStatic, 0, 0, 0)
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.draw(CamBorder, 0, 1, 0, 1, 1.05)
    love.graphics.draw(CamHUD, 0, 0, 0)
    love.graphics.print(CurrentCam, 640, 360, 0, 4, 4)
    love.graphics.setColor(1, 1, 1, 1)
end


function CamMonkeyLoad()
    MonkeyList = {} -- 3
    Baboon1List = {}
    Baboon1List.X = {300, 670, 0, 0, 310, 490, 890, nil, nil, 440, nil, nil} -- data for each of the 12 rooms (CurrentRoom)
    Baboon1List.Y = {330, 230, 400, 0, 180, 60, 100, nil, nil, 95, nil, nil} -- data for each of the 12 rooms
    Baboon1List.Scale = {0.4, 0.6, 1, 0, 0.5, 0.55, 0.3, nil, nil, 0.45, nil, nil} -- data for each of the 12 rooms
    Baboon1List.GraphicsList = {Baboon1Graphic1, Baboon1Graphic2, Baboon1Graphic3} -- Call the randomization for the asset in ChangeLocation()

    Baboon2List = {}
    Baboon2List.X = {840, 520, 840, 0, 790, -600, 190, nil, nil, nil, 440, nil} -- data for each of the 12 rooms 
    Baboon2List.Y = {380, 270, 400, 0, 180, -150, 240, nil, nil, nil, 95, nil} -- data for each of the 12 rooms
    Baboon2List.Scale = {0.75, 0.5, 0.45, 0, 0, 2, 0.5, nil, nil, nil, 0.45, nil} -- data for each of the 12 rooms
    Baboon2List.GraphicsList = {Baboon2Graphic1, Baboon2Graphic2, Baboon2Graphic3} -- Call the randomization for the asset in ChangeLocation()

    CapuchinList = {}
    CapuchinList.X = {590, 240, 410, 0, 0, 0, nil, nil, nil, nil, nil, nil} -- data for each of the 12 rooms
    CapuchinList.Y = {120, 330, 275, 0, 360, nil, nil, nil, nil, nil, nil, nil} -- data for each of the 12 rooms
    CapuchinList.Scale = {0.5, 0.4, 0.5, 0, 1, nil, nil, nil, nil, nil, nil, nil} -- data for each of the 12 rooms
    CapuchinList.GraphicsList = {CapuchinGraphic1, CapuchinGraphic2, CapuchinGraphic1Alt} -- Call the randomization for the asset in ChangeLocation()

    table.insert(MonkeyList, Baboon1List)
    table.insert(MonkeyList, Baboon2List)
    table.insert(MonkeyList, CapuchinList)

    MonkeyX = 640
    MonkeyY = 360
    MonkeyScale = 1
end

function CamMonkeyDraw() -- This function detect what cam you are on, retrieve all coordinates for all monkeys who are in this room, then, if the respective monkey is in that room, draw it with the correct X, Y and Scale

    for i = 1, 3, 1 do
        MonkeyX = MonkeyList[i].X[CurrentCam]
        MonkeyY = MonkeyList[i].Y[CurrentCam]
        MonkeyScale = MonkeyList[i].Scale[CurrentCam]

        if ActiveAI[i].CurrentRoom == CurrentCam then
            MonkeyGraphicsIndex = 1

            -- Graphics conditions for Baboon1
            if i == 1 then
                if AI.CurrentRoom == 1 or AI.CurrentRoom == 7 or AI.CurrentRoom == 10 then 
                   
                    MonkeyGraphicsIndex = 2 
                    
                    if AI.CurrentRoom == 10 and AI.Difficulty > 0 then
                        MonkeyGraphicsIndex = 1
                    end
                    
                elseif AI.CurrentRoom == 3 or AI.CurrentRoom == 6 then 
                    MonkeyGraphicsIndex = 3
                end
            end

            -- Graphics conditions for Baboon2
            if i == 2 then
                if AI2.CurrentRoom == 1 or AI2.CurrentRoom == 3 or AI2.CurrentRoom == 11 then 
                    MonkeyGraphicsIndex = 2

                    if AI2.CurrentRoom == 11 and AI.Difficulty > 0 then
                        MonkeyGraphicsIndex = 1
                    end

                elseif AI2.CurrentRoom == 2 or AI2.CurrentRoom == 7 then 
                    MonkeyGraphicsIndex = 3
                end
            end
  
            -- Graphics conditions for Capuchin
           if i == 3 then
                if AI3.CurrentRoom == 2 or AI3.CurrentRoom == 3 then 
                    MonkeyGraphicsIndex = 2
                end
           end

            love.graphics.draw(MonkeyList[i].GraphicsList[MonkeyGraphicsIndex], MonkeyX, MonkeyY, 0, MonkeyScale, MonkeyScale)
        end
    end

    if CurrentCam == 12 and CurrentCam == AI4.CurrentRoom and CamState == true then -- draws the gorilla on cam 12
        if AI4.GorillaState == 1 then
            love.graphics.draw(Gorilla, 555, 260, 0, 0.5, 0.5)
        elseif AI4.GorillaState == 2 then
            love.graphics.draw(Gorilla2, 555, 260, 0, 0.3, 0.3)
        elseif AI4.GorillaState == 3 then
            love.graphics.draw(Gorilla3, 555, 200, 0, 1.1, 1.1)
        elseif AI4.GorillaState == 4 then
            love.graphics.draw(Gorilla4, 500, 200, 0, 0.4, 0.4)
        elseif AI4.GorillaState == 5 then
            love.graphics.draw(Gorilla5, 435, 100, 0, 1.2, 1.2)
        end
    end

    if AI4.SucceededMovement == true and CamState == true then
        if AI4.RanInHallway == false then
            if CurrentCam == 6 then
                if AI4.GorillaDistanceTravelled < 600 then
                    love.graphics.draw(Gorilla, 1000 - (AI4.GorillaDistanceTravelled/2.5), 720 - AI4.GorillaDistanceTravelled, 0, 1.2, 1)
                else
                    AI4.RanInHallway = true
                end
            else
                AI4.GorillaDistanceTravelled = 0
            end
            AI4.GorillaDistanceTravelled = AI4.GorillaDistanceTravelled + AI4.GorillaSpeed
        end
    end
end

-- Misc. Functions
function love.keypressed(key) -- Controls
    if LightSwitch == false and Power > 0 then
        if key == "space" then CamState = not CamState end
    end

    if CamState == true then -- cam controls

        if key == "a" or key == "d" then
            if AI3.CurrentRoom == 1 and CurrentCam == 1 then
                
                AI3.TargetRoom = GetInitialMovementLocation3()
                ChangeLocation3(AI3.CurrentRoom, AI3.TargetRoom)
                ResetMovementOpportunity3()
            end
        end

    else -- office controls
        if key == "q" then LeftDoorClosed = not LeftDoorClosed end
        if key == "e" then RightDoorClosed = not RightDoorClosed end
        if key == "a" then 

            if (LeftDoorClosed == LeftDoorElectrified) then
                LeftDoorClosed = not LeftDoorClosed
            end

            LeftDoorElectrified = not LeftDoorElectrified 
        end

        if key == "d" then
        
            if (RightDoorClosed == RightDoorElectrified) then
                RightDoorClosed = not RightDoorClosed
            end

            RightDoorElectrified = not RightDoorElectrified 
        end

    end
    if LeftDoorClosed == false then
        LeftDoorElectrified = false
    end
    if RightDoorClosed == false then
        RightDoorElectrified = false
    end
end

function detectCollision()
    local d = distanceBetween()
end

function DrawCamButtons(CamX, CamY, TargetCam)
    if CamX < mouse_x and mouse_x < (CamX + CamButtonList.width) then
        if CamY < mouse_y and mouse_y < (CamY + CamButtonList.height) then
            if love.mouse.isDown(1) then
                CurrentCam = TargetCam
                CalendarEasterEgg = math.random(0, 100)
            end
        end
    end
end

function UpdatePowerConsumptionLevel()

    if LightSwitch == true then

        if LightSwitchIncremented == false then
            PowerConsumptionLevel = PowerConsumptionLevel + 1
        end

        LightSwitchIncremented = true
    else

        if LightSwitchIncremented == true then
            PowerConsumptionLevel = PowerConsumptionLevel - 1
        end

        LightSwitchIncremented = false
    end

    if CamState == true then

        if CamIncremented == false then
            PowerConsumptionLevel = PowerConsumptionLevel + 1
        end

        CamIncremented = true
    else

        if CamIncremented == true then
            PowerConsumptionLevel = PowerConsumptionLevel - 1
        end

        CamIncremented = false
    end
    
    if LeftDoorClosed == true then

        if LeftDoorIncremented == false then
            PowerConsumptionLevel = PowerConsumptionLevel + 0.5
        end

        LeftDoorIncremented = true
    else

        if LeftDoorIncremented == true then
            PowerConsumptionLevel = PowerConsumptionLevel - 0.5
        end

        LeftDoorIncremented = false
    end

    if LeftDoorElectrified == true then

        if LeftDoorElectricityIncremented == false then
            PowerConsumptionLevel = PowerConsumptionLevel + 0.5
        end

        LeftDoorElectricityIncremented = true
    else

        if LeftDoorElectricityIncremented == true then
            PowerConsumptionLevel = PowerConsumptionLevel - 0.5
        end

        LeftDoorElectricityIncremented = false
    end

    if RightDoorClosed == true then

        if RightDoorIncremented == false then
            PowerConsumptionLevel = PowerConsumptionLevel + 0.5
        end

        RightDoorIncremented = true
    else

        if RightDoorIncremented == true then
            PowerConsumptionLevel = PowerConsumptionLevel - 0.5
        end

        RightDoorIncremented = false
    end

    if RightDoorElectrified == true then

        if RightDoorElectricityIncremented == false then
            PowerConsumptionLevel = PowerConsumptionLevel + 0.5
        end

        RightDoorElectricityIncremented = true
    else

        if RightDoorElectricityIncremented == true then
            PowerConsumptionLevel = PowerConsumptionLevel - 0.5
        end

        RightDoorElectricityIncremented = false
    end
end