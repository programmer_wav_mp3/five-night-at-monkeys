
function endGameLoad()
    monkeyKilledBy = ""
    timerToGameOver = 3


    Jumpscare_Baboon1 = love.graphics.newImage("assets/Capuchin_Jumpscare.png")
    -- screamers_Baboon2 = love.graphics.newImage("Put baboon2")
    -- screamers_Gorilla = love.graphics.newImage("put gorilla")
    -- screamers_Gorilla = love.graphics.newImage("put gorilla")
    Jumpscare_Capuchin = love.graphics.newImage("assets/Capuchin_Jumpscare.png")

    -- screamersSound = love.audio.newSource("put sound for screamer", "static")
    -- winSound = love.audio.newSource("put a win sound")

    -- winImage = love.graphics.newImage("put win image")
end

---------------------------
function EndUpdate(dt)
    if monkeyKilledBy == "Baboon1" or monkeyKilledBy == "Baboon2" or monkeyKilledBy == "Gorilla" or monkeyKilledBy == "Capuchin" then
        timerToGameOver = timerToGameOver - dt
        if timerToGameOver <= 0 then 
           gameState = 1
        end
    end

    if Win == true then 
        timerToGameOver = timerToGameOver - dt
        if timerToGameOver <= 0 then
           gameState = 1
        end
    end
end

function EndDraw()
    love.graphics.draw(Office)
    
    if monkeyKilledBy == "Baboon1" then
        love.graphics.draw(screamers_Baboon1, 280)
        --love.audio.play(screamersSound)
    elseif monkeyKilledBy == "Baboon2" then
        --love.graphics.draw(screamers_baboon2)
        --love.audio.play(screamersSound)
    elseif monkeyKilledBy == "Gorilla" then
        --love.graphics.draw(screamers_Gorilla)
        --love.audio.play(screamersSound)
    elseif monkeyKilledBy == "Capuchin" then
        love.graphics.draw(Jumpscare_Capuchin, 280)
    end

    if monkeyKilledBy == "Capuchin" then
        love.graphics.setColor(0, 0, 0.3, 0.5)
        love.graphics.rectangle("fill", 0, 0, 1280, 720)
        love.graphics.draw(OfficeDarkness, 0, 0, 0)
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.draw(OfficeDarkness2, 0, -200, 0)
    else
        love.graphics.setColor(1, 1, 1, 0.75)
        love.graphics.draw(OfficeDarkness, 0, 0, 0)
        love.graphics.setColor(1, 1, 1, 1)
    end

    if Win == true then
        --love.graphics.draw(winImage)
        --love.audio.play(winSound)
    end
end
--------------------------
function KillPlayer(a_animatronic)
    if a_animatronic == "Baboon1" then
        monkeyKilledBy = a_animatronic
    elseif a_animatronic == "Baboon2" then
        monkeyKilledBy = a_animatronic
    elseif a_animatronic == "Gorilla" then
        monkeyKilledBy = a_animatronic
    elseif a_animatronic == "Capuchin" then
        monkeyKilledBy = a_animatronic
    end
    Dead = true
end