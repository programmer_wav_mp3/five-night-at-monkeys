function LoadBaboon()
    Baboon1Graphic1 = love.graphics.newImage("assets/Baboon1.png")
    Baboon1Graphic2 = love.graphics.newImage("assets/Baboon1_2.png")
    Baboon1Graphic3 = love.graphics.newImage("assets/Baboon1_3.png")

    ActiveAI = {}
    AI = {}
    AI.Matrix = { 
    {0, 1, 1, 0, 0, 0, 0},
    {1, 0, 1, 0, 1, 0, 0},
    {1, 1, 0, 1, 1, 0, 0},
    {0, 0, 1, 0, 0, 0, 1},
    {0, 1, 1, 0, 0, 1, 0},
    {0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 1, 0, 0, 0},
    }
    AI.Difficulty = 0
    AI.CurrentRoom = 10
    AI.TargetRoom = 1
    AI.DefaultTimer = 3
    AI.OpportunityTimer = AI.DefaultTimer
    AI.MovementTimerDefault = 3
    AI.MovementTimer = 3
    AI.SucceededMovement = false
    AI.MovedOnce = false
    table.insert(ActiveAI, AI)
end
 
function UpdateBaboon(dt)
    if Power < 0 then
        MoveOpportunity(dt)
    else
        AI.Difficulty = 0
        if AI.CurrentRoom < 0 then
            AI.CurrentRoom = 0
        end
    end
end

function DrawBaboon()
    if LightSwitch == true then
        if AI.CurrentRoom == 8 then
            love.graphics.draw(Baboon1Graphic1, -150, 150, 0, 0.5, 0.5)
        elseif AI.CurrentRoom == 9 then
            love.graphics.draw(Baboon1Graphic1, 1430, 150, 0, -0.5, 0.5)
        end
    end
end

--Update Functions--
function MoveOpportunity(dt)

    if AI.OpportunityTimer <= 0 then

        if AI.Difficulty >= love.math.random(1, 20) then
            AI.SucceededMovement = true
        else
            AI.OpportunityTimer = AI.DefaultTimer
        end

    end

    if AI.SucceededMovement == true then
        AI.MovementTimer = AI.MovementTimer - dt
    else
        if CurrentCam == AI.CurrentRoom and CamState == true then

        else
            AI.OpportunityTimer = AI.OpportunityTimer - dt
        end
    end

    if AI.MovementTimer <= 0 then
        
        if AI.CurrentRoom > 0 and AI.CurrentRoom < 6 then
            repeat
                TargetRoomTemp = love.math.random(1, 7)

                if TargetRoomTemp < AI.TargetRoom then
                    if love.math.random(1, 3) > 1 then
                        TargetRoomTemp = love.math.random(1, 7)
                    else
                        AI.TargetRoom = TargetRoomTemp
                    end
                else
                    AI.TargetRoom = TargetRoomTemp
                end

            until (AI.Matrix[AI.CurrentRoom][AI.TargetRoom] == 1) -- until the movement is a valid move according to the movement matrix

        elseif AI.CurrentRoom == 6 then

            if AI2.CurrentRoom == 8 then 
                AI.TargetRoom = 5
            else
                AI.TargetRoom = 8
            end

        elseif AI.CurrentRoom == 7 then

            if AI2.CurrentRoom == 9 then
                AI.TargetRoom = 4
            else
                AI.TargetRoom = 9
            end
           
        elseif AI.CurrentRoom == 8 then
            if LeftDoorClosed == false or LeftDoorElectrified == true then
               KillPlayer("Baboon1")
               return

            else

                AI.TargetRoom = 10

            end

        elseif AI.CurrentRoom == 9 then
            if RightDoorClosed == false or RightDoorElectrified == true then
               KillPlayer("Baboon1")
               return

            else

                AI.TargetRoom = 10

            end

        elseif AI.CurrentRoom == 10 then
            AI.TargetRoom = GetInitialMovementLocation() 
        end

        ChangeLocation(AI.CurrentRoom, AI.TargetRoom)
        ResetMovementOpportunity()
    end
end

function ResetMovementOpportunity()
    AI.DefaultTimer = (love.math.random(15, 24) / 6) -- 2.5 to 4 seconds
    AI.OpportunityTimer = AI.DefaultTimer
    AI.MovementTimer = AI.MovementTimerDefault
    AI.SucceededMovement = false
    if AI.MovedOnce == false then
        AI.MovedOnce = true
    end
end

function ChangeLocation(StartingLocation, TargetLocation)
        AI.CurrentRoom = TargetLocation
end

function GetInitialMovementLocation()
    return love.math.random(1, 2)
end

--Draw Functions--
function DrawDebug()
    love.graphics.setColor(1, 0, 0)
    love.graphics.print(AI.OpportunityTimer, 0, 0)
    love.graphics.print(AI.MovementTimer, 0, 10)
    love.graphics.print(AI.CurrentRoom, 0, 200, 0, 2, 2)
    love.graphics.print(AI.TargetRoom, 50, 200, 0, 2, 2)
    love.graphics.print(tostring(CamState), 0, 30, 0, 3, 3)
    love.graphics.print(CurrentCam, 0, 80)
    love.graphics.print(tostring(LeftDoorClosed), 640, 100, 0, 2, 2)
    love.graphics.print(tostring(RightDoorClosed), 640, 150, 0, 2, 2)

    if AI.CurrentRoom == not 0 and AI.TargetRoom == not 0 then
        love.graphics.print(tostring(AI.Matrix[AI.CurrentRoom][AI.TargetRoom]), 0, 100)
    end

    love.graphics.setColor(1, 1, 1)
end