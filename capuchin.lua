function LoadCapuchin()
    CapuchinGraphic1 = love.graphics.newImage("assets/Capuchin.png")
    CapuchinGraphic1Alt = love.graphics.newImage("assets/Capuchin_Alt.png")
    CapuchinGraphic2 = love.graphics.newImage("assets/Capuchin_2.png")
    CapuchinGraphicJumpscare = love.graphics.newImage("assets/Capuchin_Jumpscare.png")
   
    AI3 = {}    
    AI3.Matrix = { 
    {0, 1, 1, 1, 0, 0, 0},
    {1, 0, 1, 1, 1, 0, 0},
    {1, 1, 0, 1, 1, 0, 0},
    {1, 1, 1, 0, 1, 0, 0},
    {0, 1, 1, 1, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0},
    }
    AI3.Difficulty = 0
    AI3.CurrentRoom = 5
    AI3.TargetRoom = 1
    AI3.DefaultTimer = 6
    AI3.OpportunityTimer = AI3.DefaultTimer
    AI3.MovementTimerDefault = 4
    AI3.MovementTimer = 4
    AI3.SucceededMovement = false
    table.insert(ActiveAI, AI3)
end

function UpdateCapuchin(dt)
    if AI3.Difficulty > 0 and Power > 0 then
        MoveOpportunity3(dt)
    else
        if AI3.CurrentRoom > 0 then
            AI3.CurrentRoom = 0
        end
    end
end

function MoveOpportunity3(dt)

    if AI3.OpportunityTimer <= 0 then

        if AI3.Difficulty >= love.math.random(1, 20) then
            AI3.SucceededMovement = true
        else
            AI3.OpportunityTimer = AI3.DefaultTimer
        end

    end

    if AI3.SucceededMovement == true then
        AI3.MovementTimer = AI3.MovementTimer - dt
    else
        if CurrentCam == AI3.CurrentRoom and CamState == true then

        else
            AI3.OpportunityTimer = AI3.OpportunityTimer - dt
        end
    end
--
    if AI3.MovementTimer <= 0 then
        
        if AI3.CurrentRoom > 1 and AI3.CurrentRoom < 6 then
            repeat
                TargetRoomTemp3 = love.math.random(1, 7)

                if TargetRoomTemp3 > AI3.TargetRoom then
                    if love.math.random(1, 5) > 2 then
                        TargetRoomTemp3 = love.math.random(1, 7)
                    else
                        AI3.TargetRoom = TargetRoomTemp3
                    end
                else
                    AI3.TargetRoom = TargetRoomTemp3
                end

            until (AI3.Matrix[AI3.CurrentRoom][AI3.TargetRoom] == 1)

        elseif AI3.CurrentRoom == 1 then
            Power = Power - 40
            AI3.TargetRoom = GetInitialMovementLocation3()
        end

        ChangeLocation3(AI3.CurrentRoom, AI3.TargetRoom)
        ResetMovementOpportunity3()
    end
end

function ResetMovementOpportunity3()
    AI3.DefaultTimer = (love.math.random(21, 36) / 6) -- 3.5 to 6 seconds
    AI3.OpportunityTimer = AI3.DefaultTimer
    AI3.MovementTimer = AI3.MovementTimerDefault
    AI3.SucceededMovement = false
end

function ChangeLocation3(StartingLocation3, TargetLocation3)
        AI3.CurrentRoom = TargetLocation3
end

function GetInitialMovementLocation3()
        return love.math.random(4, 5)
end


function DrawDebug3()
    love.graphics.setColor(1, 0, 0)
    love.graphics.print(AI3.OpportunityTimer, 600, 400)
    love.graphics.print(AI3.MovementTimer, 600, 410)
    love.graphics.print(AI3.CurrentRoom, 600, 600, 0, 2, 2)
    love.graphics.print(AI3.TargetRoom, 650, 600, 0, 2, 2)

    if AI3.CurrentRoom == not 0 and AI3.TargetRoom == not 0 then
        love.graphics.print(tostring(AI3.Matrix[AI3.CurrentRoom][AI3.TargetRoom]), 600, 200)
    end

    love.graphics.setColor(1, 1, 1)
end
