function MenuLoad()
    LoadPreMenu()
    LoadMenu()
    LoadOption()
    
    mainMenuState = 1
    masterVolume = 1

end

function MenuUpdate(dt)
    if mainMenuState == 1 then UpdatePreMenu(dt)
    elseif  mainMenuState == 2 then UpdateMenu(dt)
    elseif mainMenuState == 3 then UpdateOption(dt)
    end
end

function MenuDraw()
    if mainMenuState == 1 then DrawPreMenu()
    elseif  mainMenuState == 2 then DrawMenu()
    elseif mainMenuState == 3 then DrawOption()
    end
end

-----PreMenu functions-----
function LoadPreMenu()
    screenSize = love.window.setMode(1208, 720)
    screenWidth = love.graphics.getWidth()
    screenHeight = love.graphics.getHeight()

    FadeOut = false
    isEnterPressed = false

    switchToMenuTimer = 2
    animationTimer = 0.2
    transitionPoint = 1

    preMenuBackground = {}
    preMenuBackground.Video = love.graphics.newVideo("assets/MenuBackground.ogv")

    PreMenuEnterText = {}
    PreMenuEnterText.Image = love.graphics.newImage("assets/PressEnter.png")
    PreMenuEnterText.Width = PreMenuEnterText.Image:getWidth()
    PreMenuEnterText.Height = PreMenuEnterText.Image:getHeight()
    PreMenuEnterText.X = screenWidth / 2 - PreMenuEnterText.Width / 2 + 40
    PreMenuEnterText.Y = screenHeight / 4 * 3

    flashingEnterSound = love.audio.newSource("assets/soundEffects/FlashingSound.ogg", "static")

end

function UpdatePreMenu(dt)
    GoToMenu(dt)
    BackgroundLoop()
end

function DrawPreMenu()

    love.graphics.draw(preMenuBackground.Video)

    if isEnterPressed == false then
        love.graphics.draw(PreMenuEnterText.Image, PreMenuEnterText.X, PreMenuEnterText.Y, 0, 1, 1)
    elseif isEnterPressed == true then
        if animationTimer >= 0.3 then
            love.graphics.draw(PreMenuEnterText.Image, PreMenuEnterText.X, PreMenuEnterText.Y, 0, 1, 1)

        elseif animationTimer <= 0 then
            animationTimer = 0.6
            love.audio.play(flashingEnterSound)
        end  
    end
end

----- Menu functions -----

function LoadMenu()
    playButtonLight = {}
    playButtonLight.Image = love.graphics.newImage("assets/Buttons/PlayButtonLight.jpeg")
    playButtonLight.X = 0
    playButtonLight.Y = 0

    buttonsWidth = playButtonLight.Image:getWidth()
    buttonsHeight = playButtonLight.Image:getHeight()
    buttonsAddY = 170

    playButtonLight.X = screenWidth / 2 - buttonsWidth / 2
    playButtonLight.Y = screenHeight / 2 - buttonsAddY
    
    playButtonBlack = {}
    playButtonBlack.Image = love.graphics.newImage("assets/Buttons/PlayButtonBlack.jpg")
    playButtonBlack.X = 0
    playButtonBlack.Y = 0
    playButtonBlack.X = screenWidth / 2 - buttonsWidth / 2
    playButtonBlack.Y = screenHeight / 2 - buttonsAddY

    optionsButtonLight = {}
    optionsButtonLight.Image = love.graphics.newImage("assets/Buttons/optionsButtonLight.jpg")
    optionsButtonLight.X = 0
    optionsButtonLight.Y = 0
    optionsButtonLight.X = screenWidth / 2 - buttonsWidth / 2
    optionsButtonLight.Y = screenHeight / 2

    optionsButtonBlack = {}
    optionsButtonBlack.Image = love.graphics.newImage("assets/Buttons/optionsButtonBlack.jpg")
    optionsButtonBlack.X = 0
    optionsButtonBlack.Y = 0
    optionsButtonBlack.X = screenWidth / 2 - buttonsWidth / 2
    optionsButtonBlack.Y = screenHeight / 2

    quitButtonLight = {}
    quitButtonLight.Image = love.graphics.newImage("assets/Buttons/QuitButtonLight.jpg")
    quitButtonLight.X = 0
    quitButtonLight.Y = 0
    quitButtonLight.X = screenWidth / 2 - buttonsWidth / 2
    quitButtonLight.Y = screenHeight / 2 + buttonsAddY

    quitButtonBlack = {}
    quitButtonBlack.Image = love.graphics.newImage("assets/Buttons/QuitButtonBlack.jpg")
    quitButtonBlack.X = 0
    quitButtonBlack.Y = 0
    quitButtonBlack.X = screenWidth / 2 - buttonsWidth / 2
    
    quitButtonBlack.Y = screenHeight / 2 + buttonsAddY

    DifficultyButtonUp1X = 1020
    DifficultyButtonUp1Y = 70
    DifficultyButtonUp2X = 1020
    DifficultyButtonUp2Y = 270
    DifficultyButtonUp3X = 1020
    DifficultyButtonUp3Y = 470
    DifficultyButtonUp4X = 1020
    DifficultyButtonUp4Y = 670

    DifficultyButtonDown1X = 920
    DifficultyButtonDown1Y = 70
    DifficultyButtonDown2X = 920
    DifficultyButtonDown2Y = 270
    DifficultyButtonDown3X = 920
    DifficultyButtonDown3Y = 470
    DifficultyButtonDown4X = 920
    DifficultyButtonDown4Y = 670

end

function UpdateMenu(dt)
    LerpToSwitchScene(dt, 2)
    BackgroundLoop()
    UpdateButtons()
end

function DrawMenu()
    love.graphics.setColor(1, 1, 1, transitionPoint)
    love.graphics.draw(preMenuBackground.Video)
    
    if playButtonBlackActive == false then 
        love.graphics.draw(playButtonLight.Image, playButtonLight.X, playButtonLight.Y )
    elseif playButtonBlackActive == true then
        love.graphics.draw(playButtonBlack.Image, playButtonBlack.X, playButtonBlack.Y )
    end
    
    if optionsButtonBlackActive == false then
        love.graphics.draw(optionsButtonLight.Image, optionsButtonLight.X, optionsButtonLight.Y)
    elseif optionsButtonBlackActive == true then
        love.graphics.draw(optionsButtonBlack.Image, optionsButtonBlack.X, optionsButtonBlack.Y)
    end
    
    if  quitButtonBlackActive == false then
        love.graphics.draw(quitButtonLight.Image, quitButtonLight.X, quitButtonLight.Y )
    elseif quitButtonBlackActive == true then
        love.graphics.draw(quitButtonBlack.Image, quitButtonBlack.X, quitButtonBlack.Y )
    end

    love.graphics.print("Baboon1 Difficulty", 890, 10, 0, 2, 2)
    love.graphics.print(AI.Difficulty, 985, 40, 0, 2, 2)

    love.graphics.setColor(1, 0, 0)
    love.graphics.rectangle("fill", 920, 70, 60, 25)
    love.graphics.setColor(0, 1, 0)
    love.graphics.rectangle("fill", 1020, 70, 60, 25)
    love.graphics.setColor(1, 1, 1)

    love.graphics.print("Baboon2 Difficulty", 890, 210, 0, 2, 2)
    love.graphics.print(AI2.Difficulty, 985, 240, 0, 2, 2)

    love.graphics.setColor(1, 0, 0)
    love.graphics.rectangle("fill", 920, 270, 60, 25)
    love.graphics.setColor(0, 1, 0)
    love.graphics.rectangle("fill", 1020, 270, 60, 25)
    love.graphics.setColor(1, 1, 1)

    love.graphics.print("Capuchin Difficulty", 885, 410, 0, 2, 2)
    love.graphics.print(AI3.Difficulty, 985, 440, 0, 2, 2)

    love.graphics.setColor(1, 0, 0)
    love.graphics.rectangle("fill", 920, 470, 60, 25)
    love.graphics.setColor(0, 1, 0)
    love.graphics.rectangle("fill", 1020, 470, 60, 25)
    love.graphics.setColor(1, 1, 1)

    love.graphics.print("Gorilla Difficulty", 905, 610, 0, 2, 2)
    love.graphics.print(AI4.Difficulty, 985, 640, 0, 2, 2)

    love.graphics.setColor(1, 0, 0)
    love.graphics.rectangle("fill", 920, 670, 60, 25)
    love.graphics.setColor(0, 1, 0)
    love.graphics.rectangle("fill", 1020, 670, 60, 25)
    love.graphics.setColor(1, 1, 1)

end

----- Options Functions -----

function LoadOption()
    slideBar = {}
    slideBar.Image = love.graphics.newImage("assets/Buttons/slideBar.png")
    slideBar.Width = slideBar.Image:getWidth()
    slideBar.Height = slideBar.Image:getHeight()
    slideBar.X = screenWidth / 2 - slideBar.Width / 2
    slideBar.Y = screenHeight / 2 - slideBar.Height / 2

    pointSlide = {}
    pointSlide.Image = love.graphics.newImage("assets/Buttons/pointForSlideBar.png")
    pointSlide.Width = pointSlide.Image:getWidth()
    pointSlide.Height = pointSlide.Image:getHeight()
    pointSlide.X = slideBar.X + slideBar.Width - pointSlide.Width
    pointSlide.Y = slideBar.Y

    volumeText = {}
    volumeText.Image = love.graphics.newImage("assets/Buttons/volumeText.png")
    volumeText.Width = volumeText.Image:getWidth()
    volumeText.Height = volumeText.Image:getWidth()
    volumeText.X = screenWidth / 2 - volumeText.Width / 2 
    volumeText.Y = (screenHeight / 2 - volumeText.Height / 2) - 50

    backText = {}
    backText.Image = love.graphics.newImage("assets/Buttons/BackText.png")
    backText.Width = backText.Image:getWidth()
    backText.Height = backText.Image:getHeight()
    backText.X = screenWidth / 2 - backText.Width / 2 
    backText.Y = (screenHeight - backText.Height / 2) - 50  

    volumeValue = 0.95
end

function UpdateOption(dt)
    mouseX, mouseY = love.mouse.getPosition()
    if love.keyboard.isDown("return") == true then mainMenuState = 1 end

    BackgroundLoop()
    VolumeBar()
end

function DrawOption()
    love.graphics.setColor(1, 1, 1, transitionPoint)
    love.graphics.draw(preMenuBackground.Video)

    love.graphics.draw(slideBar.Image, slideBar.X, slideBar.Y)
    love.graphics.draw(pointSlide.Image, pointSlide.X, pointSlide.Y)
    love.graphics.draw(volumeText.Image, volumeText.X, volumeText.Y)  
    love.graphics.draw(backText.Image, backText.X, backText.Y)    
end

----- buttons -----

function UpdateButtons()
    mouseX, mouseY = love.mouse.getPosition()
    
    -- play button --
    playButtonBlackActive = true
    if mouseX > playButtonLight.X and mouseX < playButtonLight.X + buttonsWidth then
        if mouseY > playButtonLight.Y and mouseY < playButtonLight.Y + buttonsHeight then 
            playButtonBlackActive = false
            if love.mouse.isDown(1) then
                FadeOut = true
            end
        end
    end
    -- Option Button --
    optionsButtonBlackActive = true
    if mouseX > optionsButtonLight.X and mouseX < optionsButtonLight.X + buttonsWidth then
        if mouseY > optionsButtonLight.Y and mouseY < optionsButtonLight.Y + buttonsHeight then 
            optionsButtonBlackActive = false
            if love.mouse.isDown(1) then
                mainMenuState = 3
            end
        end
    end
    -- Quit Button --
    quitButtonBlackActive = true
    if mouseX > quitButtonLight.X and mouseX < quitButtonLight.X + buttonsWidth then
        if mouseY > quitButtonLight.Y and mouseY < quitButtonLight.Y + buttonsHeight then
            quitButtonBlackActive = false
            if love.mouse.isDown(1) then
                love.event.quit()
            end
        end
    end
end

function love.mousepressed(x, y, button, istouch)
    -- Difficulty Buttons --
    if mainMenuState == 2 and gameState == 1 then

       if mouseX > DifficultyButtonUp1X and mouseX < DifficultyButtonUp1X + 60 then
            if mouseY > DifficultyButtonUp1Y and mouseY < DifficultyButtonUp1Y + 25 then
                if button == 1 then
                    AI.Difficulty = AI.Difficulty + 1
                end
            end
       end

       if mouseX > DifficultyButtonDown1X and mouseX < DifficultyButtonDown1X + 60 then
            if mouseY > DifficultyButtonDown1Y and mouseY < DifficultyButtonDown1Y + 25 then
                if button == 1 then
                    AI.Difficulty = AI.Difficulty - 1
                end
            end
       end

       if mouseX > DifficultyButtonUp2X and mouseX < DifficultyButtonUp2X + 60 then
            if mouseY > DifficultyButtonUp2Y and mouseY < DifficultyButtonUp2Y + 25 then
                if button == 1 then
                    AI2.Difficulty = AI2.Difficulty + 1
                end
            end
       end

       if mouseX > DifficultyButtonDown2X and mouseX < DifficultyButtonDown2X + 60 then
            if mouseY > DifficultyButtonDown2Y and mouseY < DifficultyButtonDown2Y + 25 then
                if button == 1 then
                    AI2.Difficulty = AI2.Difficulty - 1
                end
            end
       end

       if mouseX > DifficultyButtonUp3X and mouseX < DifficultyButtonUp3X + 60 then
            if mouseY > DifficultyButtonUp3Y and mouseY < DifficultyButtonUp3Y + 25 then
                if button == 1 then
                    AI3.Difficulty = AI3.Difficulty + 1
                end
            end
       end

       if mouseX > DifficultyButtonDown3X and mouseX < DifficultyButtonDown3X + 60 then
            if mouseY > DifficultyButtonDown3Y and mouseY < DifficultyButtonDown3Y + 25 then
                if button == 1 then
                    AI3.Difficulty = AI3.Difficulty - 1
                end
            end
       end

       if mouseX > DifficultyButtonUp4X and mouseX < DifficultyButtonUp4X + 60 then
            if mouseY > DifficultyButtonUp4Y and mouseY < DifficultyButtonUp4Y + 25 then
                if button == 1 then
                    AI4.Difficulty = AI4.Difficulty + 1
                end
            end
       end

       if mouseX > DifficultyButtonDown4X and mouseX < DifficultyButtonDown4X + 60 then
            if mouseY > DifficultyButtonDown4Y and mouseY < DifficultyButtonDown4Y + 25 then
                if button == 1 then
                    AI4.Difficulty = AI4.Difficulty - 1
                end
            end
       end

       if AI2.Difficulty > 20 then
            AI2.Difficulty = 20
       elseif AI2.Difficulty < 0 then
            AI2.Difficulty = 0
       end

       if AI2.Difficulty > 20 then
            AI2.Difficulty = 20
       elseif AI2.Difficulty < 0 then
            AI2.Difficulty = 0
       end

       if AI3.Difficulty > 20 then
            AI3.Difficulty = 20
       elseif AI3.Difficulty < 0 then
            AI3.Difficulty = 0
       end

       if AI4.Difficulty > 20 then
            AI4.Difficulty = 20
       elseif AI4.Difficulty < 0 then
            AI4.Difficulty = 0
       end
    end
end
----- background loop -----

function BackgroundLoop()
    if preMenuBackground.Video:isPlaying() == false then
        preMenuBackground.Video:rewind()  
        preMenuBackground.Video:play()
    end
end

----- timers when enter is pressed in pre menu -----

function GoToMenu(dt)
    if love.keyboard.isDown("return") == true then isEnterPressed = true end
    
    if isEnterPressed == true then
        animationTimer = animationTimer - dt
        switchToMenuTimer = switchToMenuTimer - dt
    end
    if switchToMenuTimer <= 0 then mainMenuState = 2 end
end

----- Lerp animation to change scene -----

function TransitionLerp(dt, a_multiplicator)
    transitionPoint = transitionPoint - dt * a_multiplicator 
end

function LerpToSwitchScene(dt, a_gameState)
    if FadeOut == true then 
        TransitionLerp(dt, 0.7)
        if transitionPoint <= 0 then
            FadeOut = false
            transitionPoint = 1
            gameState = a_gameState
        end 
    end
end

----- To change Volume -----
function VolumeBar()
    if mouseX > slideBar.X and mouseX < slideBar.X + slideBar.Width then
        if mouseY > slideBar.Y and mouseY < slideBar.Y + slideBar.Height then 
            if love.mouse.isDown(1) then
                pointSlide.X = mouseX - (pointSlide.Width / 2)
            end
        end
    end
    volumeValue = (pointSlide.X - slideBar.X) / 500
    love.audio.setVolume(volumeValue)
    print(volumeValue)
end