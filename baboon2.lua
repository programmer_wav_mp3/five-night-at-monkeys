function LoadBaboon2()
    Baboon2Graphic1 = love.graphics.newImage("assets/Baboon2.png")
    Baboon2Graphic2 = love.graphics.newImage("assets/Baboon2_2.png")
    Baboon2Graphic3 = love.graphics.newImage("assets/Baboon2_3.png")
   
    AI2 = {}
    AI2.Matrix = { 
    {0, 1, 1, 0, 0, 0, 0},
    {1, 0, 1, 0, 1, 0, 0},
    {1, 1, 0, 1, 1, 0, 0},
    {0, 0, 1, 0, 0, 0, 1},
    {0, 1, 1, 0, 0, 1, 0},
    {0, 0, 0, 0, 1, 0, 0},
    {0, 0, 0, 1, 0, 0, 0},
    }
    AI2.Difficulty = 0
    AI2.CurrentRoom = 11
    AI2.TargetRoom = 1
    AI2.DefaultTimer = 2.5
    AI2.OpportunityTimer = AI2.DefaultTimer
    AI2.MovementTimerDefault = 3.5
    AI2.MovementTimer = 3.5
    AI2.SucceededMovement = false
    AI2.MovedOnce = false
    table.insert(ActiveAI, AI2)
end
 
function UpdateBaboon2(dt)
    if Power < 0 then
        MoveOpportunity2(dt)
    else
        AI2.Difficulty = 0
        if AI2.CurrentRoom < 0 then
            AI2.CurrentRoom = 0
        end
    end
end

function DrawBaboon2()

    if LightSwitch == true then
        if AI2.CurrentRoom == 8 then
            love.graphics.draw(Baboon2Graphic1, -150, 150, 0, 0.5, 0.5)
        elseif AI2.CurrentRoom == 9 then
            love.graphics.draw(Baboon2Graphic1, 1430, 150, 0, -0.5, 0.5)
        end
    end
end

--Update Functions--
function MoveOpportunity2(dt)

    if AI2.OpportunityTimer <= 0 then

        if AI2.Difficulty >= love.math.random(1, 20) then
            AI2.SucceededMovement = true
        else
            AI2.OpportunityTimer = AI2.DefaultTimer
        end

    end

    if AI2.SucceededMovement == true then
        AI2.MovementTimer = AI2.MovementTimer - dt
    else
        if CurrentCam == AI2.CurrentRoom and CamState == true then

        else
            AI2.OpportunityTimer = AI2.OpportunityTimer - dt
        end
    end

    if AI2.MovementTimer <= 0 then
        
        if AI2.CurrentRoom > 0 and AI2.CurrentRoom < 6 then
            repeat
                TargetRoomTemp2 = love.math.random(1, 7)

                if TargetRoomTemp2 < AI2.TargetRoom then
                    if love.math.random(1, 3) > 1 then
                        TargetRoomTemp2 = love.math.random(1, 7)
                    else
                        AI2.TargetRoom = TargetRoomTemp2
                    end
                else
                    AI2.TargetRoom = TargetRoomTemp2
                end

            until (AI2.Matrix[AI2.CurrentRoom][AI2.TargetRoom] == 1)

        elseif AI2.CurrentRoom == 6 then

            if AI.CurrentRoom == 8 then
                AI2.TargetRoom = 5
            else
                AI2.TargetRoom = 8
            end

        elseif AI2.CurrentRoom == 7 then 

            if AI.CurrentRoom == 9 then
                AI2.TargetRoom = 4
            else
                AI2.TargetRoom = 9
            end

        elseif AI2.CurrentRoom == 8 then
            if LeftDoorClosed == false or LeftDoorElectrified == false then
               KillPlayer("Baboon2")
               return

            else
            AI2.TargetRoom = 11

            end

        elseif AI2.CurrentRoom == 9 then
            if RightDoorClosed == false or RightDoorElectrified == false then
               KillPlayer("Baboon2")
               return

            else
            AI2.TargetRoom = 11

            end

        elseif AI2.CurrentRoom == 11 then
            AI2.TargetRoom = GetInitialMovementLocation2() 
        end

        ChangeLocation2(AI2.CurrentRoom, AI2.TargetRoom)
        ResetMovementOpportunity2()
    end
end

function ResetMovementOpportunity2()
    AI2.DefaultTimer = (love.math.random(9, 27) / 6) -- 1.5 to 4.5 seconds
    AI2.OpportunityTimer = AI2.DefaultTimer
    AI2.MovementTimer = AI2.MovementTimerDefault
    AI2.SucceededMovement = false
    if AI2.MovedOnce == false then
        AI2.MovedOnce = true
    end
end

function ChangeLocation2(StartingLocation2, TargetLocation2)
        AI2.CurrentRoom = TargetLocation2
end

function GetInitialMovementLocation2()
    Baboon2StartingMovement = love.math.random(1, 2)

    if Baboon2StartingMovement == 1 then
        return 1
    elseif Baboon2StartingMovement == 2 then
        return 4
    end
end

--Draw Functions--
function DrawDebug2()
    love.graphics.setColor(1, 0, 0)
    love.graphics.print(AI2.OpportunityTimer, 600, 0)
    love.graphics.print(AI2.MovementTimer, 600, 10)
    love.graphics.print(AI2.CurrentRoom, 600, 200, 0, 2, 2)
    love.graphics.print(AI2.TargetRoom, 650, 200, 0, 2, 2)

    if AI2.CurrentRoom == not 0 and AI2.TargetRoom == not 0 then
        love.graphics.print(tostring(AI2.Matrix[AI2.CurrentRoom][AI2.TargetRoom]), 600, 100)
    end

    love.graphics.setColor(1, 1, 1)
end